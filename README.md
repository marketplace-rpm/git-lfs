# Information / Информация

SPEC-файл для создания RPM-пакета **git-lfs**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/fedora-git-lfs`.
2. Установить пакет: `dnf install git-lfs`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)